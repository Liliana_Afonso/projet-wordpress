<!-- START: PAGE CONTENT -->
<div class="row animate-up">
    <div class="col-sm-8">
        <main class="post-single">
            <article class="post-content section-box">
                <div class="post-inner">
                    <header class="post-header">
                        <div class="post-data">
                            <div class="post-tag">
                                <a href="<?php the_permalink() ?>">#Image</a>
                                <a href="<?php the_permalink() ?>">#Architect</a>
                            </div>

                            <div class="post-title-wrap">
                                <h1 class="post-title"><?php the_title() ?></h1>
                                <time class="post-datetime" datetime=<?php echo get_the_date("c") ?>>
                                    <span class="day"><?php echo get_the_date("d") ?></span>
                                    <span class="month"><?php echo get_the_date("M") ?></span>
                                </time>
                            </div>

                            <div class="post-info">
                                <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-user"></i><?php the_author() ?></a>
                                <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i> <?php echo get_comments_number() ?></a>
                            </div>
                        </div>
                    </header>

                    <div class="post-editor clearfix">
                        <?php the_excerpt(); ?><br>
                            <cite><a href="<?php the_permalink() ?>"><?php the_author_meta('first_name'); ?></a> - Someone famous in Source Title</cite>
                        

                        <figure id="attachment_905" style="width: 509px;" class="wp-caption alignright">
                            <img class="size-full wp-image-905 " title="Image Alignment 509x319" alt="Image Alignment 509x319" src="<?php echo get_post_thumbnail_id() ?>" width="509" height="319">

                            <?php $imageData = wp_get_attachment_image_src(get_post_thumbnail_id($post_ID), 'medium');
                            echo $imageData[0]; ?>

                            <figcaption class="wp-caption-text">Feels good to be right all the time.</figcaption>
                        </figure>

                    </div>

                    <footer class="post-footer">
                        <div class="post-share">
                            <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-503b5cbf65c3f4d8" async="async"></script>
                            <div class="addthis_sharing_toolbox"></div>
                        </div>
                    </footer>
                </div><!-- .post-inner -->
            </article><!-- .post-content -->

            <nav class="post-pagination section-box">
                <div class="post-next">
                    <div class="post-tag">Previous Article <a href="<?php the_permalink() ?>">#Tagapple</a></div>
                    <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php previous_post(); ?> </a></h3>

                    <div class="post-info">
                        <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-user"></i><?php the_author_meta('first_name'); ?></a>
                        <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number() ?></a>
                    </div>
                </div>
                <div class="post-prev">
                    <div class="post-tag">Next Article <a href="<?php the_permalink() ?>">#Photography</a></div>
                    <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php next_post(); ?></a></h3>

                    <div class="post-info">
                        <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-user"></i><?php the_author_meta('first_name'); ?></a>
                        <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number() ?></a>
                    </div>
                </div>
            </nav><!-- .post-pagination -->

            <div class="post-comments">
                <h2 class="section-title">Comments <?php echo get_comments_number() ?></h2>

                <div class="section-box">
                    <ol class="comment-list">
                        <li class="comment">
                            <article class="comment-body">
                                <div class="comment-avatar">
                                    <img src="<?php echo get_bloginfo('template_directory');
                                                echo get_avatar($id_or_email, $size = '96', $default = '<path_to_url>');
                                                ?>" alt="avatar" />
                                </div>
                                <div class="comment-content">
                                    <div class="comment-meta">
                                        <span class="name"><?php the_author_meta('first_name'); ?></span>
                                        <time class="date" datetime="2015-03-20T13:00:14+00:00"> <?php the_time('F j, Y g:i a') ?></time>
                                        <a class="reply-link" href="<?php echo get_bloginfo('template_directory'); ?>/<?php the_permalink() ?>#comment-reply">Reply</a>
                                    </div>
                                    <div class="comment-message">
                                        <p>Lorem ipsum dolor sit aum nulla quis nesciunt ipsa aliquam aliquid eum, voluptatibus
                                            assumenda minima vel. Eaque, velit architecto error ducimus aliquid.</p>
                                    </div>
                                </div>
                            </article>

                        </li><!-- .comment -->
                    </ol><!-- .comment-list -->

                    <div id="comment-reply" class="comment-reply">
                        <form>
                            <div class="input-field">
                                <input type="text" name="rs-comment-name" />
                                <span class="line"></span>
                                <label>Name *</label>
                            </div>

                            <div class="input-field">
                                <input type="email" name="rs-comment-email" />
                                <span class="line"></span>
                                <label>Email *</label>
                            </div>

                            <div class="input-field">
                                <input type="text" name="rs-comment-website" />
                                <span class="line"></span>
                                <label>Website</label>
                            </div>

                            <div class="input-field">
                                <textarea rows="4" name="rs-comment-message"></textarea>
                                <span class="line"></span>
                                <label>Type Comment Here *</label>
                            </div>

                            <div class="text-right">
                                <span class="btn-outer btn-primary-outer ripple">
                                    <input class="btn btn-lg btn-primary" type="button" value="Leave Comment">
                                </span>
                            </div>
                        </form>
                    </div><!-- .comment-reply -->
                </div><!-- .section-box -->
            </div><!-- .post-comments -->
        </main>
        <!-- .post-single -->
    </div>
    <!--debut sidebar========================================== -->

    <div class="col-sm-4">
        <div class="sidebar sidebar-default">
            <div class="widget-area">
                <aside class="widget widget-profile">
                    <div class="profile-photo">
                        <img src="<?php echo get_avatar(get_the_author_meta('ID'), 32); ?>" alt="" />
                    </div>
                    <div class="profile-info">
                        <h2 class="profile-title"><?php the_author() ?></h2>
                        <h3 class="profile-position"><?php the_author_meta('description'); ?></h3>
                    </div>
                </aside><!-- .widget-profile -->

                <aside class="widget widget_search">
                    <h2 class="widget-title">Search</h2>
                    <form class="search-form">
                        <label class="ripple">
                            <span class="screen-reader-text">Search for:</span>
                            <input class="search-field" type="search" placeholder="Search">
                        </label>
                        <input type="submit" class="search-submit" value="Search">
                    </form>
                </aside><!-- .widget_search -->

                <aside class="widget widget_contact">
                    <h2 class="widget-title">Contact Me</h2>
                    <form class="contactForm" action="https://rscard.px-lab.com/html/php/contact_form.php" method="post">
                        <div class="input-field">
                            <input class="contact-name" type="text" name="name" />
                            <span class="line"></span>
                            <label>Name</label>
                        </div>

                        <div class="input-field">
                            <input class="contact-email" type="email" name="email" />
                            <span class="line"></span>
                            <label>Email</label>
                        </div>

                        <div class="input-field">
                            <input class="contact-subject" type="text" name="subject" />
                            <span class="line"></span>
                            <label>Subject</label>
                        </div>

                        <div class="input-field">
                            <textarea class="contact-message" rows="4" name="message"></textarea>
                            <span class="line"></span>
                            <label>Message</label>
                        </div>

                        <span class="btn-outer btn-primary-outer ripple">
                            <input class="contact-submit btn btn-lg btn-primary" type="submit" value="Send" />
                        </span>

                        <div class="contact-response"></div>
                    </form>
                </aside><!-- .widget_contact -->

                <aside class="widget widget-popuplar-posts">
                    <h2 class="widget-title">Popular posts</h2>
                    <ul>
                        <li>
                            <div class="post-media"><a href="<?php the_permalink() ?>"><img src="<?php the_permalink() ?>" alt=""> </a>
                                <?php if (has_post_thumbnail())
                                    the_post_thumbnail(); ?>
                            </div>
                            <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                            <div class="post-info"><a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number() ?></a></div>
                        </li>
                    </ul>
                </aside><!-- .widget-popuplar-posts -->

                <aside class="widget widget_tag_cloud">
                    <h2 class="widget-title">Tag Cloud</h2>
                    <div class="tagcloud">
                        <?php wp_tag_cloud('orderby'); ?>

                    </div>
                </aside><!-- .widget_tag_cloud -->

                <aside class="widget widget-recent-posts">
                    <h2 class="widget-title">Recent posts</h2>

                    <h3 class="post-title"><a href="<?php the_permalink() ?>">
                            <ul id="slider-id" class="slider-class">
                                <?php
                                $recent_posts = wp_get_recent_posts(array(
                                    'numberposts' => 3, // Number of recent posts thumbnails to display
                                    'post_status' => 'publish' // Show only the published posts
                                ));
                                foreach ($recent_posts as $post) : ?>
                                    <li>
                                        <div class="post-tag">
                                            <a href="<?php the_permalink() ?>"><?php get_posts('category') ?></a>
                                        </div>
                                        <a href="<?php echo get_permalink($post['ID']) ?>">
                                            <p class="slider-caption-class"><?php echo $post['post_title'] ?></p>
                                        </a>
                                        <div class="post-info"><a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number() ?></a></div>

                                    </li>
                                <?php endforeach;
                            wp_reset_query(); ?>
                            </ul>
                    </h3>

                </aside><!-- .widget-recent-posts -->

                <aside class="widget widget_categories">
                    <h2 class="widget-title">Categories</h2>
                    <ul>
                        <li><a href="<?php the_permalink() ?>" title="Architecture Category Posts">Architecture</a> (<?php echo get_comments_number() ?>)</li>

                    </ul>
                </aside><!-- .widget_categories -->
            </div><!-- .widget-area -->
        </div><!-- .sidebar -->
    </div>