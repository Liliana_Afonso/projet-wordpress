<?php get_header(); ?>

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php get_template_part('front-page'); ?>
            <?php endwhile;
    endif; ?>

        
    <?php get_footer(); ?>
</div><!-- .wrapper -->

<a class="btn-scroll-top" href="<?php the_permalink() ?>#"><i class="rsicon rsicon-arrow-up"></i></a>
<div id="overlay"></div>
<div id="preloader">
    <div class="preload-icon"><span></span><span></span></div>
    <div class="preload-text">Loading ...</div>
</div>

<!-- Scripts -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_directory'); ?>/js/site.js"></script>
</body>

</html>