<?php get_header(); ?>

<div class="content">
    <div class="container">
        <div class="blog">
            <div class="blog-grid">
                <div class="grid-sizer"></div>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php get_template_part('content'); ?>
                    <?php endwhile;
            endif; ?>

                <!--   </div> .col-xs-6 -->
            </div><!-- .blog-grid -->

            <div class="pagination">
                <?php the_posts_pagination(array(
                    'mid_size' => 4,
                    'prev_text' => __('Précédent', 'textdomain'),
                    'next_text' => __('Suivant', 'textdomain'),
                )); ?>

            </div><!-- .pagination -->
        </div><!-- .blog -->
        <!-- END: PAGE CONTENT -->

        <?php get_sidebar(); ?>


    </div><!-- .container -->
</div><!-- .content -->

<?php get_footer(); ?>

</div><!-- .wrapper -->

<a class="btn-scroll-top" href="<?php the_permalink() ?>"><i class="rsicon rsicon-arrow-up"></i></a>
<div id="overlay"></div>
<div id="preloader">
    <div class="preload-icon"><span></span><span></span></div>
    <div class="preload-text">Loading ...</div>
</div>

<!-- Scripts -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_directory'); ?>/js/site.js"></script>
<?php wp_footer() ?>
</body>

</html>