<div class="profile-photo">
    <!-- <img src="<?php echo get_bloginfo('template_directory'); ?>/img/uploads/rs-photo-v2.jpg" alt="" />-->
    <img src="<?php the_permalink() ?>" alt="">

    <?php if (has_post_thumbnail())
        the_post_thumbnail(); ?>


</div>
<div class="profile-info">
    <h2 class="profile-title"><?php the_author_meta('first_name'); ?></h2>
    <h3 class="profile-position"><?php the_author_meta('description'); ?></h3>
</div>
</aside><!-- .widget-profile -->

<aside class="widget widget_search animate-up">
    <h2 class="widget-title">Search</h2>
    <form class="search-form">
        <label class="ripple">
            <span class="screen-reader-text">Search for:</span>
            <input class="search-field" type="search" placeholder="Search">
        </label>
        <input type="submit" class="search-submit" value="Search">
    </form>
</aside><!-- .widget_search -->

<aside class="widget widget_contact animate-up">
    <h2 class="widget-title">Contact Me</h2>
    <form class="contactForm" action="https://rscard.px-lab.com/html/php/contact_form.php" method="post">
        <div class="input-field">
                            <input class="contact-name" type="text" name="name" />
                            <span class="line"></span>
                            <label>Name</label>
                        </div>

                        <div class="input-field">
                            <input class="contact-email" type="email" name="email" />
                            <span class="line"></span>
                            <label>Email</label>
                        </div>

                        <div class="input-field">
                            <input class="contact-subject" type="text" name="subject" />
                            <span class="line"></span>
                            <label>Subject</label>
                        </div>

                        <div class="input-field">
                            <textarea class="contact-message" rows="4" name="message"></textarea>
                            <span class="line"></span>
                            <label>Message</label>
                        </div>

        <span class="btn-outer btn-primary-outer ripple">
            <input class="contact-submit btn btn-lg btn-primary" type="submit" value="Send" />
        </span>

        <div class="contact-response"></div>
    </form>
</aside><!-- .widget_contact -->

<aside class="widget widget-popuplar-posts">
                    <h2 class="widget-title">Popular posts</h2>
                    <ul>
                        <li>
                            <div class="post-media"><a href="<?php the_permalink() ?>"><img src="<?php the_permalink() ?>" alt=""> </a>
                                <?php if (has_post_thumbnail())
                                    the_post_thumbnail(); ?>
                            </div>
                            <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                            <div class="post-info"><a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number() ?></a></div>
                        </li>
                    </ul>
                </aside><!-- .widget-popuplar-posts -->

                <aside class="widget widget_tag_cloud">
                    <h2 class="widget-title">Tag Cloud</h2>
                    <div class="tagcloud">
                        <?php wp_tag_cloud('orderby'); ?>

                    </div>
                </aside><!-- .widget_tag_cloud -->

                <aside class="widget widget-recent-posts">
                    <h2 class="widget-title">Recent posts</h2>

                    <h3 class="post-title"><a href="<?php the_permalink() ?>">
                            <ul id="slider-id" class="slider-class">
                                <?php
                                $recent_posts = wp_get_recent_posts(array(
                                    'numberposts' => 3, // Number of recent posts thumbnails to display
                                    'post_status' => 'publish' // Show only the published posts
                                ));
                                foreach ($recent_posts as $post) : ?>
                                    <li>
                                        <div class="post-tag">
                                            <a href="<?php the_permalink() ?>"><?php get_posts('category') ?></a>
                                        </div>
                                        <a href="<?php echo get_permalink($post['ID']) ?>">
                                            <p class="slider-caption-class"><?php echo $post['post_title'] ?></p>
                                        </a>
                                        <div class="post-info"><a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number() ?></a></div>

                                    </li>
                                <?php endforeach;
                            wp_reset_query(); ?>
                            </ul>
                    </h3>

                </aside><!-- .widget-recent-posts -->

                <aside class="widget widget_categories">
                    <h2 class="widget-title">Categories</h2>
                    <ul>
                        <li><a href="<?php the_permalink() ?>" title="Architecture Category Posts">Architecture</a> (<?php echo get_comments_number() ?>)</li>

                    </ul>
                </aside><!-- .widget_categories -->
            </div><!-- .widget-area -->
        </div><!-- .sidebar -->
    </div>